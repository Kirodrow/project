<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/bootstrap.css',
        'css/bootstrap.min.css',
        'css/bootstrap-theme.css',
        'css/font-awesome.css',
        'css/font-awesome.min.css',
        'css/bootstrap-theme.min.css',
        'css/coming-soon-social.css',
        'css/icomoon-social.css',
        'css/leaflet.css',
        'css/main-green.css',
        'css/main-grey.css',
        'css/main-orange.css',
        'css/main-red.css',
        'css/main.css',
    ];
    public $fonts = [
       /* 'fonts/glyphicons-halflings-regular.eot',
        'fonts/glyphicons-halflings-regular.svg',
        'fonts/glyphicons-halflings-regular.ttf',
        'fonts/glyphicons-halflings-regular.woff',
        'fonts/glyphicons-halflings-regular.woff2',*/
    ];
    public $js = [

        //'js/bootstrap.js',
        //  'js/bootstrap.min.js',
        'js/jquery.bxslider.js',
        'js/jquery.fitvids.js',
        'js/jquery.sequence.js',
        'js/jquery.sequence-min.js',
       //'js/jquery-1.9.1.min.js',
       // 'js/modernizr-2.6.2-respond-1.1.0.min.js',

        'js/jquery.accordion.js',
        'js/jquery.cookie.js',
        'js/main-menu.js',
        'js/ajax.js',
        'js/template.js',
        'http://cdn.leafletjs.com/leaflet-0.5.1/leaflet.js',
    ];
    public $jsOptions = [];//'position' => \yii\web\View::POS_HEAD];
    public $depends = [

        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
