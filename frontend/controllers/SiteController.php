<?php
namespace frontend\controllers;


//use function Symfony\Component\Debug\Tests\FatalErrorHandler\test_namespaced_function;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Mailer;
use app\models\Type;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use app\models\Book;
use app\models\Rubric;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;


/**
 * Site controller
 */
class SiteController extends Controller
{
    public $layout = 'basic';
    public $mass = array();

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /* 'access' => [
                 'class' => AccessControl::className(),
                 'only' => ['logout', 'signup'],
                 'rules' => [
                     [
                         'actions' => ['signup'],
                         'allow' => true,
                         'roles' => ['?'],
                     ],
                     [
                         'actions' => ['logout'],
                         'allow' => true,
                         'roles' => ['@'],
                     ],
                 ],
             ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $data = Book::find()->asArray()->all();
        foreach ($data as $value) {
            $mass[] = $value['name'];
        }

        if (Yii::$app->request->isAjax &&  $_POST['Book']['name'] != NULL) {
            $data = Book::find()->asArray()->where('like', 'name', $_POST['Book']['name'])->all();
            foreach ($data as $value) {
                $mass[] = $value['name'];
            }
            // Формируем массив для JSON ответа
            //debug($_POST);
            $result =
                $_POST['Book']['name'];

            $res = Book::findOne(['name' => $result]);

            $result = 'Автор Книги: ' . $res->author . ' , Название книги: ' . $res->name;
            return (json_encode($result));
        }
        elseif(Yii::$app->request->isAjax &&  $_POST['Book']['name'] == NULL) {
            $result = 'введите строку поиска';
            return (json_encode($result));
        }
        $model = new Book();
        $model2 = new Book();
        $table = new Rubric();



        $base = Book::find()->asArray()->With('rubric')->all();



        return $this->render('index', compact('model', 'model2','base', 'table', 'mass'));
    }


    public function actionGroup()
    {
        if (Yii::$app->request->isAjax &&  $_POST['Book']['name'] != NULL) {

            // Формируем массив для JSON ответа
            //debug($_POST);
            $result =
                $_POST['Book']['name'];

            $res = Book::findOne(['name' => $result]);

            $result = 'Автор Книги: ' . $res->author . ' , Название книги: ' . $res->name;
            return (json_encode($result));
        }
        else{
            $result = 'введите строку поиска';
            return (json_encode($result));
        }


    }


    public function actionNew()
    {

        $data = Book::find()->asArray()->all();
        foreach ($data as $value) {
            $mass[] = $value['name'];
        }

        $model = new Book();
        $model2 = new Book();
        $table = new Rubric();

        $base = Book::find()->asArray()->With('rubric')->all();

        return $this->render('new', compact('model', 'model2','base', 'table', 'mass'));
    }


    public function actionShow()
    {
        $res = Book::find()->asArray()->all();
//debug($res);
    $result = '';
foreach ($res as $value){

    $result = $result . '<br>'.'Автор Книги: ' . $value['author'] . ' , Название книги: ' . $value['name'];
}
            return ($result);


    }
    public function actionSearch()
    {
        $res = Book::find()->asArray()->all();

        debug($_POST);
return $res;


    }



}




