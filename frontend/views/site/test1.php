<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Group;
use yii\helpers\ArrayHelper;

?>
<style type="text/css">
    table {
        background: white; /* Цвет фона таблицы */


    }
    td, th {
        background: white; /* Цвет фона ячеек */
        text-align: center;
    }
    div.section{
        color: maroon;
    }
    h2{
        color: maroon;
    }

</style>




    <div class="section">
    <div class="container">
    <h2>Заполнение расписания</h2>
    <div class="row">
    <!-- Pricing Plans Wrapper -->
    <div class="pricing-wrapper col-md-12">
    <!-- Pricing Plan -->

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?php

    $session = Yii::$app->session;
    $session->open();
    foreach ($session['post']['group'] as $item => $value){?>
        <?php $i = 0; foreach ($session['post']['day'] as  $day){?>

        <h2>№ Группы <?=$value . ','?> день <?= $session['post']['day'][$i]. ','; ?>
            неделя <?=1 + $session['post']['week'][0]. '.';?></h2>

        <table>
            <caption>Распиание</caption>
            <tr>
                <th align="center">пара №</th>
                <th>Предмет</th>

                <th>Вид пары</th>
                <th>Аудитория</th>

            </tr>
            <tr align="center">
<?php for ($y = 1; $y <= 6; $y++){?>
                <td><?php echo $y;?></td>
                <td> <?= $form->field($table, 'lesson['.$value.']['.$day.']['.$y.'][subject]')->dropDownList
                    (ArrayHelper::map(\app\models\Teacher::find()->all(), 'suname', 'suname'))->label(false) ?></td>


                <td>

                    <?= $form->field($table, 'lesson['.$value.']['.$day.']['.$y.'][lessonType]')->dropDownList([
                        'Лк' => 'лекция',
                        'Лр' => 'лаба',
                        'Пр' => 'практическое',

                    ])->label(false)?>
                </td>
                <td>
                    <?= $form->field($table, 'lesson['.$value.']['.$day.']['.$y.'][auditory]')->label(false)?>
                </td>


            </tr>
<?php }?>




        </table>
        <?php $i++;}?>

    <?php }?>
<br>
        <?= Html::submitButton('сохранить временно результат', ['class' => 'btn btn-primary',
            'type' => 'submit']);?>
        <?php ActiveForm::end() ?>


        </div>
        <!-- End Pricing Plans Wrapper -->
        </div>
        </div>
        </div>










