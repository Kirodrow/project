<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Group;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>
<style type="text/css">
    table {
        background: white; /* Цвет фона таблицы */
        color: white; /* Цвет текста */
    }
    td {
        background: white; /* Цвет фона ячеек */
    }
</style>
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>



<div class="section">
    <div class="container">
        <h2>Заполнение расписания</h2>
        <div class="row">
            <!-- Pricing Plans Wrapper -->
            <div class="pricing-wrapper col-md-12">
                <!-- Pricing Plan -->
                <div class="pricing-plan">
                    <!-- Pricing Plan Ribbon -->
                    <div class="ribbon-wrapper">

                    </div>
                    <?= $form->field($model, 'group')->dropDownList
                    (ArrayHelper::map(Group::find()->all(), 'number', 'number'),
                        [
                            //'prompt' => 'Выберите один вариант',
                            'multiple' => 'true',
                            // 'style' => ' width: 300px; height: 65px'
                        ]) ?>
                </div>
                <!-- End Pricing Plan -->
                <div class="pricing-plan">
                    <?= $form->field($model, 'week')
                        ->dropDownList([
                            '0' => 'Неделя №1',
                            '1' => 'Неделя №2',
                            '2' => 'Неделя №3',
                            '3' => 'Неделя №4',

                        ],
                            [
                                //'prompt' => 'Выберите один вариант',
                                'multiple' => 'true',
                                //'style' => ' width: 300px; height: 65px'
                            ]);?>
                </div>
                <!-- Promoted Pricing Plan -->
                <div class="pricing-plan">
                    <?= $form->field($model, 'day')
                        ->dropDownList([
                            'понедельник' => 'понедельник',
                            'вторник' => 'вторник',
                            'среда' => 'среда',
                            'четверг' => 'четверг',
                            'пятница' => 'пятница',
                            'суббота' => 'суббота',

                        ],
                            [
                                //'prompt' => 'Выберите один вариант',
                                'multiple' => 'true',
                                // 'style' => ' width: 300px; height: 65px'
                            ]);?>
                </div>
            </div>
            <?= Html::submitButton('Начать заполнять таблицу', ['class' => 'btn btn-primary',
                'type' => 'submit']) ?>

            <?php ActiveForm::end() ?>
            <script>
                function confirmDelete() {

                    if (confirm("Вы подтверждаете удаление?")) {

                    } else {

                        return false;

                    }

                }
            </script>

            <?= Html::a('Добавить группу или предмет', Url::to('/group'), ['class' => 'btn btn-primary'])?>
            <?= Html::a('cкачать json', Url::to('/json'), ['class' => 'btn btn-primary'])?>
            <?= Html::a('Очистить все данные', Url::to('/delete'), ['class' => 'btn btn-primary', 'onclick'=>"return confirmDelete();"])?>



            <!--
    End Pricing Plans Wrapper -->
        </div>
    </div>
</div>








